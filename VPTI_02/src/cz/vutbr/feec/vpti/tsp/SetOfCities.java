package cz.vutbr.feec.vpti.tsp;

import java.util.HashMap;

public class SetOfCities {
	
	private int SumOfCities = 0;
	
	private HashMap<Integer, City> cities = new HashMap<>();
	
	public void addCity(String name, double x, double y){
		City city = new City(name, x, y);
		cities.put(SumOfCities,city);
		SumOfCities++;
	}
	
	public double getDistance(int id1, int id2) {
		City c1 = cities.get(id1);
		City c2 = cities.get(id2);
		
		double distance =  Math.sqrt(Math.pow((c2.getX()-c1.getX()),2) + Math.pow((c2.getX()-c1.getY()),2));;
		
		System.out.println(distance);
		
		return distance;
	}
	
	public int getSumOfCities() {
		return cities.size();
	}
	
}