package cz.vutbr.feec.vpti.tsp;

import org.jgap.DeltaFitnessEvaluator;
import org.jgap.Gene;
import org.jgap.Genotype;
import org.jgap.IChromosome;
import org.jgap.InvalidConfigurationException;

import java.util.List;

import org.jgap.Chromosome;
import org.jgap.Configuration;
import org.jgap.impl.DefaultConfiguration;
import org.jgap.impl.IntegerGene;
import org.jgap.impl.SwappingMutationOperator;

public class Runner {
	
	public static void main(String[] args) throws InvalidConfigurationException {
		SetOfCities setOfCities = new SetOfCities();
		
		setOfCities.addCity("Brno", 78, 32);
		setOfCities.addCity("Kromeriz", 8, 2);
		setOfCities.addCity("Olomouc", 48, 98);
		setOfCities.addCity("Budejice", 31, 65);
		setOfCities.addCity("Prostejov", 58, 24);
		setOfCities.addCity("Pardubice", 21, 25);
		setOfCities.addCity("Prerov", 54, 29);
		
		Configuration gaConf = new DefaultConfiguration();
		
		Configuration.resetProperty(Configuration.PROPERTY_FITEVAL_INST);
		gaConf.setFitnessEvaluator(new DeltaFitnessEvaluator());
		
		gaConf.setPopulationSize(50);
		
		gaConf.getGeneticOperators().clear();
		SwappingMutationOperator swapper = new SwappingMutationOperator(gaConf);
		gaConf.addGeneticOperator(swapper);
		
		int chrome_size = setOfCities.getSumOfCities();
		
		IChromosome sampleChromosome = new Chromosome(gaConf, new IntegerGene(gaConf), chrome_size);
		gaConf.setSampleChromosome(sampleChromosome);
		
		gaConf.setPreservFittestIndividual(true);
		gaConf.setKeepPopulationSizeConstant(false);
		
		MyFitness myFitness = new MyFitness(setOfCities);
		gaConf.setFitnessFunction(myFitness);
		Genotype genotype = Genotype.randomInitialGenotype(gaConf);
		
		List chromosomes = genotype.getPopulation().getChromosomes();
		for(Object chromosome : chromosomes) {
			IChromosome chrom = (IChromosome) chromosome;
			
			for(int j = 0; j<chrom.size(); j++) {
				Gene gene = chrom.getGene(j);
				gene.setAllele(j);
			}
		}
		
		for(int i = 0; i<100;i++) {
			genotype.evolve();
			double fittest = genotype.getFittestChromosome().getFitnessValue();
			System.out.println(fittest);
		}
		
		IChromosome best = genotype.getFittestChromosome();
		System.out.println(best);
	
	}

}
