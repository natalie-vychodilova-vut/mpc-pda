package cz.vutbr.feec.vpti.tsp;

import org.jgap.FitnessFunction;
import org.jgap.IChromosome;

public class MyFitness extends FitnessFunction {

	private SetOfCities setOfCities;
	
	public MyFitness( SetOfCities setOfCities) {
		super();
		this.setOfCities = setOfCities;
	}
	
	@Override
	protected double evaluate(IChromosome route) {
		int sumOfCities = route.size();
		double finalDistance = 0;
		
		for (int i = 0; i < sumOfCities-1; i++) {
			int id1 = (int) route.getGene(i).getAllele();
			int id2 = (int) route.getGene(i+1).getAllele();
			
			double distance = setOfCities.getDistance(id1, id2);
			finalDistance += distance;
		}
		return finalDistance;
	}
}
