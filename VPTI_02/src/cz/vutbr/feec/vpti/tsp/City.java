package cz.vutbr.feec.vpti.tsp;

public class City {
	
	private String name;
	private double x;
	private double y;
	
	public City(String name, double x, double y) {
		super();
		this.name = name;
		this.x = x;
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}