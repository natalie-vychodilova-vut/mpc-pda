package sample;

import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import robocode.HitByBulletEvent;

public class TankBoss extends Robot{
public void run() {
while (true) {
fire(1.621966568123786);
turnLeft(12.777061993364853);
fire(2.459632539925611);
turnRight(265.09401673743724);
turnGunRight(353.45693980545354);
ahead(-15.649226330547673);
ahead(6.811159807716649);
fire(3.9081385516247202);
}
}
public void onScannedRobot(ScannedRobotEvent e) {
fire(2.9367037247403416);
turnGunRight(60.05420272994521);
turnRight(70.80690994448504);
fire(0.40753888195996346);
turnGunLeft(159.32568843406457);
turnGunRight(101.00259595391124);
}
public void onHitRobot(HitRobotEvent event) {
fire(1.7555022448056539);
turnRight(109.55769301109333);
turnGunRight(206.90802974196336);
ahead(-0.7230487170807454);
turnGunRight(110.82313009281957);
turnGunRight(239.88163290588514);
}
}
