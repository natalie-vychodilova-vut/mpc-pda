package cz.vutbr.feec.vpti.examples;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.function.Multiply;
import org.jgap.gp.function.Add;
import org.jgap.gp.function.Subtract;
import org.jgap.gp.impl.DeltaGPFitnessEvaluator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.jgap.gp.terminal.Terminal;
import org.jgap.gp.terminal.Variable;

import cz.vutbr.feec.vpti.jgap.SimpleMathTestFitnessFunction;

public class P02_GenerateRandomPrograms {
	public static void main(String[] args) throws InvalidConfigurationException {
		GPConfiguration config = new GPConfiguration();
		config.setMaxInitDepth(4);
		config.setPopulationSize(10);
		
		Class[] returnTypes = { CommandGene.IntegerClass };
		
		Class[][] argTypes = { {} };
		
		Variable xVariable = new Variable(config, "X", CommandGene.IntegerClass);
		Variable yVariable = new Variable(config, "Y", CommandGene.IntegerClass);
		CommandGene[][] nodeSets = { 
				{ 
					xVariable,
					yVariable,
					new Subtract(config, CommandGene.IntegerClass),
					new Multiply(config, CommandGene.IntegerClass),
					new Add(config, CommandGene.IntegerClass),
					new Terminal(config, CommandGene.IntegerClass, 0.0, 10.0)
				}
		};
		
		// tady je ulozena jedna populace a informace genotypu
		GPGenotype result = GPGenotype.randomInitialGenotype(
				config, 
				returnTypes, 
				argTypes, 
				nodeSets, 
				20, 
				true);
		
		// tady tahle funkce ziskava chromozon - chci ziskat index dane populace
		IGPProgram randomProgram = result.getGPPopulation().getGPProgram(5);
		System.out.println(randomProgram.toStringNorm(0));
	}
}
