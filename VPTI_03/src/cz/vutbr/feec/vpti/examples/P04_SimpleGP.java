package cz.vutbr.feec.vpti.examples;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.impl.DeltaGPFitnessEvaluator;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.jgap.gp.terminal.Terminal;
import org.jgap.gp.terminal.Variable;

import cz.vutbr.feec.vpti.jgap.MojePlus;
import cz.vutbr.feec.vpti.jgap.SimpleMathTestFitnessFunction;

import org.jgap.gp.CommandGene;
import org.jgap.gp.GPProblem;
import org.jgap.gp.function.Add;
import org.jgap.gp.function.Multiply;
import org.jgap.gp.function.Subtract;

public class P04_SimpleGP extends GPProblem {
	
	private final Integer[] INPUT_X =  { 26, 8, 20, 33, 37 };
	private final Integer[] INPUT_Y =  { 35, 24, 1, 11, 16 };
	private final int[] OUTPUT = 	 { 829, 141, 467, 1215, 1517 };
	
	private Variable _xVariable;
	private Variable _yVariable;
	
	public P04_SimpleGP() throws InvalidConfigurationException {
		super(new GPConfiguration());
		GPConfiguration config = getGPConfiguration();
		config.setGPFitnessEvaluator(new DeltaGPFitnessEvaluator());
		config.setMaxInitDepth(4);
		config.setPopulationSize(1000);
		
		_xVariable = Variable.create(config, "X", CommandGene.IntegerClass);
		_yVariable = Variable.create(config, "Y", CommandGene.IntegerClass);
		
		SimpleMathTestFitnessFunction fitness = new SimpleMathTestFitnessFunction(INPUT_X, INPUT_Y, OUTPUT, _xVariable, _yVariable);
		config.setFitnessFunction(fitness);
	}

	@Override
	public GPGenotype create() throws InvalidConfigurationException {
		GPConfiguration config = getGPConfiguration();
	
		Class[] returnTypes = { CommandGene.IntegerClass };
		
		Class[][] argTypes = { {} };
		
		
		CommandGene[][] nodeSets = { 
				{ 
					_xVariable,
					_yVariable,
					new Subtract(config, CommandGene.IntegerClass),
					new Multiply(config, CommandGene.IntegerClass),
					new MojePlus(config, CommandGene.IntegerClass),
					new Terminal(config, CommandGene.IntegerClass, 0.0, 10.0)
				}
		};
		
		GPGenotype result = GPGenotype.randomInitialGenotype(
				config, 
				returnTypes, 
				argTypes, 
				nodeSets, 
				20, 
				true);
		
		return result;
	}
	
	public static void main(String[] args) throws InvalidConfigurationException {
		P04_SimpleGP gp = new P04_SimpleGP();
		GPGenotype population = gp.create();
		
		population.evolve(50);
		population.outputSolution(population.getAllTimeBest());
	}
}

