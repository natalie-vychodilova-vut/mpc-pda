package cz.vutbr.feec.vpti.examples;

public class P01_FitnessFunction {
	public static void main(String[] args) {
		Integer[] INPUT_X =  { 26, 8, 20, 33, 37 };
		Integer[] INPUT_Y =  { 35, 24, 1, 11, 16 };
		int[] OUTPUT = 	 { 829, 141, 467, 1215, 1517 };
		
		long totalError = 0;
		
		//TODO - compute the total error
		for (int i = 0; i < OUTPUT.length; i++) {
			long result = OUTPUT[i] - executeProgram(INPUT_X[i], INPUT_Y[i]);
			totalError += result;
		}

        System.out.println("TOTAL ERROR: "+ totalError);
	}

	private static long executeProgram(int x, int y) {
		//return x + y;
		return 2*x + y;
	}
}


