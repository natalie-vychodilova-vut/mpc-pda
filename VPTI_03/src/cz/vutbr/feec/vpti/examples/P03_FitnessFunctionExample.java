package cz.vutbr.feec.vpti.examples;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.function.Multiply;
import org.jgap.gp.function.Add;
import org.jgap.gp.function.Subtract;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.GPGenotype;
import org.jgap.gp.impl.GPProgram;
import org.jgap.gp.terminal.Terminal;
import org.jgap.gp.terminal.Variable;

import cz.vutbr.feec.vpti.jgap.SimpleMathTestFitnessFunction;

public class P03_FitnessFunctionExample extends GPProgram {
	public P03_FitnessFunctionExample() throws Exception {
		super();
	}

	public static void main(String[] args) throws InvalidConfigurationException {
		IGPProgram program = getRandomProgram();
		
		// 
		Integer[] INPUT_X =  { 26, 8, 20, 33, 37 };
		Integer[] INPUT_Y =  { 35, 24, 1, 11, 16 };
		int[] OUTPUT = 	 { 829, 141, 467, 1215, 1517 };
		Variable xVariable = Variable.create(program.getGPConfiguration(), "X", CommandGene.IntegerClass);
		Variable yVariable = Variable.create(program.getGPConfiguration(), "Y", CommandGene.IntegerClass);
		SimpleMathTestFitnessFunction fitFunc = new SimpleMathTestFitnessFunction(INPUT_X, INPUT_Y, OUTPUT, xVariable, yVariable);
		
		double val = fitFunc.getFitnessValue(program);
		
		System.out.println("Program: "+program.toStringNorm(0));
		System.out.println("Fitness: "+val);
		
		// Program: ((X * X) + (5 - X)) - ((X * 5) * (5 - 5))
		// Fitness: 670.0
		
	}

	// From example 02
	private static IGPProgram getRandomProgram() throws InvalidConfigurationException {
		GPConfiguration config = new GPConfiguration();
		config.setMaxInitDepth(4);
		config.setPopulationSize(7);

		Class[] returnTypes = { CommandGene.IntegerClass };

		Class[][] argTypes = { {} };

		Variable _xVariable = Variable.create(config, "X", CommandGene.IntegerClass);
		Variable _yVariable = Variable.create(config, "Y", CommandGene.IntegerClass);
		CommandGene[][] nodeSets = { 
				{
					_xVariable, 
					_yVariable, 
					new Subtract(config, CommandGene.IntegerClass),
					new Multiply(config, CommandGene.IntegerClass),
					new Add(config, CommandGene.IntegerClass),
					new Terminal(config, CommandGene.IntegerClass, 0.0, 10.0, true) } };

		GPGenotype result = GPGenotype.randomInitialGenotype(config, returnTypes, argTypes, nodeSets, 20, true);

		IGPProgram randomProgram = result.getGPPopulation().getGPProgram(3);
		return randomProgram;
	}

}
