package cz.vutbr.feec.vpti.jgap;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.MathCommand;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.ProgramChromosome;

public class MojePlus extends MathCommand {

	public MojePlus(GPConfiguration a_conf, Class a_returnType) throws InvalidConfigurationException {
		super(a_conf, 2, a_returnType);
	}

	@Override
	public String toString() {
		return "&1 + &2";
	}

	public double execute_double(ProgramChromosome c, int n, Object[] args) {
		return c.execute_double(n, 0, args) + c.execute_double(n, 1, args);
	}
	
	public int execute_int(ProgramChromosome c, int n, Object[] args) {
		return c.execute_int(n, 0, args) + c.execute_int(n, 1, args);
	}

}
