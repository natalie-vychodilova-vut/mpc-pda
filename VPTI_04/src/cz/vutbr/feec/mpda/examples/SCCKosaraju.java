package cz.vutbr.feec.mpda.examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class SCCKosaraju {

	public static List<List<MyVertex>> scc(MyGraph graph) {
		
		// 1) compute start and finish indexes (see dfsRecursive method below). 
		for (MyVertex vertex : graph.getVertexList()) {
			if(!vertex.isUsed()) {
				dfsRecursive(vertex, true);
			}
		}
		
		// 2) create reverse graph (see T02_ReverseGraphExample)
		MyGraph reversedGraph = reverseGraph(graph);
		List<MyVertex> vertexList = reversedGraph.getVertexList();
		
		
		// 3) sort vertices by finish index (descending), see "sortByFinalIndexDesc" method below
		sortByFinalIndexDesc(vertexList);
		
		
		// 4) find SCC using DFS (see dfsRecursive) and add SCC to result
		List<List<MyVertex>> result = new ArrayList<>();
		for (MyVertex vertex : vertexList) {
			if(vertex.isUsed()) {
				List<MyVertex> component = dfsRecursive(vertex, false);
				result.add(component);
			}
		}
		return result;
	}
	
    private static void sortByFinalIndexDesc(List<MyVertex> vertexList) {
		Collections.sort(vertexList, new Comparator<MyVertex>() {
			public int compare(MyVertex o1, MyVertex o2) {
				return -Integer.compare(o1.getFinish(), o2.getFinish());
			}
		});
	}

	private static MyGraph reverseGraph(MyGraph graph) {
		MyGraph reversedGraph = new MyGraph();
		for (MyVertex v : graph.getVertexList()) {
			for (MyVertex v2 : v.getNeighbours()) {
				reversedGraph.addNode(v2.getId(), v.getId());
			}
		}
		return reversedGraph;
	}

	/** 
     * Depth First Search - recursive version
     * 
     * @param vertexList
     * @param whereToStart vertex
     * @return list of reachable nodes
     */
    public static LinkedList<MyVertex> dfsRecursive(MyVertex v, boolean changeTimer){
        LinkedList<MyVertex> result = new LinkedList<>();
        result.add(v);
        v.setUsed(true);
        v.setStart(++orderCounter);
        
        // iterate over all neighbours
        for (int j = 0; j < v.getNeighbours().size(); j++) {
        	MyVertex n = v.getNeighbours().get(j);
        	if(!n.isUsed()) {
        		result.addAll(dfsRecursive(n, changeTimer));
        	}
		}
        v.setFinish(++orderCounter);
        return result;
    }
    
    private static int orderCounter = 0;
}
