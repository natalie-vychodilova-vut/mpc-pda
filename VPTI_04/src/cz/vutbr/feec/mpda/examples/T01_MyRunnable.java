package cz.vutbr.feec.mpda.examples;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class T01_MyRunnable {
	public static void main(String[] args) {
		MyGraph g = new MyGraph();
		g.addNode(1, 2);
		g.addNode(6, 1);
		g.addNode(2, 3);
		g.addNode(7, 1);
		g.addNode(7, 6);
		
		g.traversalDFSRecursive(1);
		
		List<MyVertex> list = g.getVertexList();
		Collections.sort(list, new Comparator<MyVertex>() {
			public int compare(MyVertex o1, MyVertex o2) {
				return -Integer.compare(o1.getFinish(), o2.getFinish());
			}
			
		});
		
		
	}
}
