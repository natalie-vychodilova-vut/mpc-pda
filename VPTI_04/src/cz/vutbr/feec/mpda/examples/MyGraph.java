package cz.vutbr.feec.mpda.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class MyGraph {
	private Map<Integer, MyVertex> vertices = new HashMap<>();
	public void addNode(int u1Id, int u2Id) {
		MyVertex v1 = getExistingOrCreateNew(u1Id);
		MyVertex v2 = getExistingOrCreateNew(u2Id);
		
		v1.addNeighbour(v2);
		//v2.addNeighbour(v1);
	}
	
	private MyVertex getExistingOrCreateNew(int id) {
		MyVertex v = vertices.get(id);
		if(v == null) {
			v = new MyVertex();
			v.setId(id);
			vertices.put(id, v);
		}
		return v;
	}

	public MyVertex getVertex(int id) {
		return vertices.get(id);
	}

	public void traversalBFS(int nodeToStartWith) {
		LinkedList<MyVertex> list = new LinkedList<>();
		list.addFirst(vertices.get(nodeToStartWith));
		
		HashSet<MyVertex> visited = new HashSet<>();
		visited.add(vertices.get(nodeToStartWith));
		
		while (!list.isEmpty()) {
			MyVertex v = list.removeLast();
			System.out.print(v.getId()+"   ");
			
			for (MyVertex neighbour : v.getNeighbours()) {
				if(!visited.contains(neighbour)) {
					list.addFirst(neighbour);
					visited.add(neighbour);
				}
			}
		}
		System.out.println();
	}

	public void traversalDFS(int nodeToStartWith) {
		System.out.println("TODO");
	}

	public List<MyVertex> getVertexList() {
		//List<MyVertex> valueList = new ArrayList<MyVertex>(vertices.values());
		//return valueList;
		
		return new LinkedList<MyVertex>(vertices.values());
	}

	public void traversalDFSRecursive(int i) {
		traversalDFSRecursive(vertices.get(i));
	}
	
	private int counter = 0;

	private void traversalDFSRecursive(MyVertex v) {
		System.out.println("START " + v.getId() + " Counter: " + counter);
		v.setStart(counter++);
		v.setUsed(true);
		for (MyVertex n : v.getNeighbours()) {
			if(!n.isUsed()) {
				n.setUsed(true);
				traversalDFSRecursive(n);
			}
		}
		v.setFinish(counter++);
		System.out.println("FINISH " + v.getId() + " Counter: " + counter);
	}

	@Override
	public String toString() {
		StringBuffer str =  new StringBuffer();
		for (MyVertex v: getVertexList()) {
			str.append(v.getId()+"->"+v.getNeighbours());
		}
		// [[1], [0], [0, 1]]
		return str.toString();
	}
}
