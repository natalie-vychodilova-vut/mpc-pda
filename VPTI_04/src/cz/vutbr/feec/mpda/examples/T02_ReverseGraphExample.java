package cz.vutbr.feec.mpda.examples;

public class T02_ReverseGraphExample {
	public static void main(String[] args) {
		MyGraph graph = new MyGraph();
		graph.addNode(1, 2);
		graph.addNode(6, 1);
		graph.addNode(2, 3);
		graph.addNode(7, 1);
		graph.addNode(7, 6);
		System.out.println(graph);
		
		MyGraph reversedGraph = new MyGraph();
		for (MyVertex v : graph.getVertexList()) {
			for (MyVertex v2 : v.getNeighbours()) {
				reversedGraph.addNode(v2.getId(), v.getId());
			}
			
		}
		System.out.println(reversedGraph);
	}
}


// PROJEKT: geneticke programovani nebo zpetnovazebni uceni (temito dvema zpusoby vytvorit)
// - vysledky robotu prezentovat, obhajoba v poslednim tydnu 