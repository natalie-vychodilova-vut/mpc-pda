package cz.vutbr.feec.pda.hungarian.examples;

import cz.vutbr.feec.pda.hungarian.model.Item;
import cz.vutbr.feec.pda.hungarian.model.Matrix;
import cz.vutbr.feec.pda.hungarian.model.Row;

public class E07_MarkRedLines {
	public static void main(String[] args) {
		int[][] data = new int[][] {
			{40, 60, 15}, 
			{25, 30, 45}, 
			{55, 30, 25}
		};
		
		Matrix m = new Matrix(data);
		
		m.reduceStep1();
		//System.out.println(m);
		m.reduceStep2();
		//System.out.println(m);
		
		for (int row = 0; row < data.length; row++) {
			for (int col = 0; col < data.length; col++) {
				Row r = m.getRow(row);
				Item i = r.getItem(col);
				if(i.getValue() == 0 && !i.isRedLine()  ) {
					// TODO mark vertical or horizontal line
					//System.out.println("COL: " + i.getColumn().countZeros());
					//System.out.println("ROW: " + i.getRow().countZeros());
					
					System.out.println("Pracovnik: " + i.getRow() + " ukol: " + i.getColumn());
					
					if(i.getColumn().countZeros() > i.getRow().countZeros()) {						
						i.getColumn().setRedLine(true);
					}
					else {
						i.getRow().setRedLine(true);						
					}
				}
			}
		}
		System.out.println(m);
	}
}
