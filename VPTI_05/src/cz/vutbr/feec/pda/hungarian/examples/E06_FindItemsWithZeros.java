package cz.vutbr.feec.pda.hungarian.examples;

import cz.vutbr.feec.pda.hungarian.model.Item;
import cz.vutbr.feec.pda.hungarian.model.Matrix;
import cz.vutbr.feec.pda.hungarian.model.Row;

public class E06_FindItemsWithZeros {
	public static void main(String[] args) {
		int[][] data = new int[][] {
			{25, 40, 0}, 
			{0, 0, 20}, 
			{30, 0, 0}
		};
		Matrix m = new Matrix(data);
		
		// TODO find a column or row with max free zeros
		for(int r=0; r<data.length; r++) {
			for(int c=0; c<data.length;c++) {
				Row row = m.getRow(r);
				Item i = row.getItem(c);
				if(i.getValue() == 0) {
					System.out.println(row.getItem(c));
				}
			}
		}
	}
}
