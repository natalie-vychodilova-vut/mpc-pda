package cz.vutbr.feec.pda.hungarian.examples;

import cz.vutbr.feec.pda.hungarian.model.Matrix;

public class E01_Matrix {
	public static void main(String[] args) {
		int[][] data = new int[][] {
			{4, 3, 3}, 
			{6, 4, 5}, 
			{7, 7, 5}
		};
		Matrix m = new Matrix(data);
		// Prints the whole matrix
		System.out.println(m);
		
		// Prints item (starts from 0,0)
		System.out.println(m.getItem(2, 1));
		
		// Prints row (index starts from 0)
		System.out.println(m.getRow(0));
		
		// Prints column (index starts from 0)
		System.out.println(m.getColumn(0));
		
		m.setItem(1,2, 10);
		
		System.out.println(m);
		
	}
}

